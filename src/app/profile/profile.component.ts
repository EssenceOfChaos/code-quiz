import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth/auth.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  pageTitle = 'Profile';
  profile: any;

  constructor(public auth: AuthService, private title: Title) {}

  ngOnInit() {
    if (this.auth.userProfile) {
      this.profile = this.auth.userProfile;
    } else {
      this.auth.getProfile((err, profile) => {
        this.profile = profile;
      });
    }
    this.title.setTitle(this.pageTitle);
  }
}
