import { QuizModule } from './quiz.module';

describe('MaterialModule', () => {
  let quizModule: QuizModule;

  beforeEach(() => {
    quizModule = new QuizModule();
  });

  it('should create an instance', () => {
    expect(quizModule).toBeDefined();
  });
});
