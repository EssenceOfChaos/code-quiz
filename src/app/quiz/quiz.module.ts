import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuizComponent } from './lib/quiz.component';
import { QuizRoutingModule } from './quiz.routing.module';

@NgModule({
  imports: [CommonModule, QuizRoutingModule],
  declarations: [QuizComponent]
})
export class QuizModule {}
