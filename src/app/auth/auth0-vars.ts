interface AuthConfig {
  clientID: string;
  domain: string;
  callbackURL: string;
  scope: string;
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: '4hIrEHhEvnNJIlQAt8JUbI4r8HOoftJA',
  domain: 'swiftlabs.auth0.com',
  callbackURL: 'http://localhost:4200/callback',
  scope: 'openid profile'
};